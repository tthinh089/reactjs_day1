import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div>
        <div className="container row w-100 m-auto">
          <div className="col-sm-12 col-md-6 col-lg-4 col-xl-3 text-center border p-0 my-3">
            <img
              src="https://via.placeholder.com/824x552"
              alt=""
              className="w-100"
            />
            <h4 className="p-2">Card title</h4>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil,
              quae!
            </p>
            <hr className="hr hr-blurry" />
            <button className="btn btn-primary mb-">Find Out More!</button>
          </div>
          <div className="col-sm-12 col-md-6 col-lg-4 col-xl-3 text-center border p-0 my-3">
            <img
              src="https://via.placeholder.com/824x552"
              alt=""
              className="w-100"
            />
            <h4 className="p-2">Card title</h4>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil,
              quae!
            </p>
            <hr className="hr hr-blurry" />
            <button className="btn btn-primary mb-3">Find Out More!</button>
          </div>
          <div className="col-sm-12 col-md-6 col-lg-4 col-xl-3 text-center border p-0 my-3">
            <img
              src="https://via.placeholder.com/824x552"
              alt=""
              className="w-100"
            />
            <h4 className="p-2">Card title</h4>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil,
              quae!
            </p>
            <hr className="hr hr-blurry" />
            <button className="btn btn-primary mb-3">Find Out More!</button>
          </div>{" "}
          <div className="col-sm-12 col-md-6 col-lg-4 col-xl-3 text-center border p-0 my-3">
            <img
              src="https://via.placeholder.com/824x552"
              alt=""
              className="w-100"
            />
            <h4 className="p-2">Card title</h4>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nihil,
              quae!
            </p>
            <hr className="hr hr-blurry" />
            <button className="btn btn-primary mb-3">Find Out More!</button>
          </div>
        </div>
      </div>
    );
  }
}
