import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div>
        <div className="footer bg-dark text-center text-light p-4 mt-4">
          Copyright © Your Website 2023
        </div>
      </div>
    );
  }
}
