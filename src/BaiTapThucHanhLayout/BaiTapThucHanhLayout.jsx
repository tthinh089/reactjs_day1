import React, { Component } from "react";
import Header from "../Header/Header";
import Banner from "../Body/Banner";
import Item from "../Body/Item";
import Footer from "../Footer/Footer";

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <Item />
        <Footer />
      </div>
    );
  }
}
